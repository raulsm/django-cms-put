from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .models import Contenido

# Create your views here.
@csrf_exempt
def get_content(request,clave):
    if request.method == "PUT":
        valor = request.body.decode("utf-8")
        new_contenido = Contenido(clave=clave, valor=valor)
        new_contenido.save()

    try:
        contenido = Contenido.objects.get(clave=clave)
        return HttpResponse(contenido.valor)
    except Contenido.DoesNotExist:
        return HttpResponse('No existe contenido para la clave' + clave)

def index(request):
    return HttpResponse("Pagina principal")