from django.urls import path
from . import views

urlpatterns = [
    path("<str:clave>", views.get_content),
    path("", views.index)
]